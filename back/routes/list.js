const express = require('express')
const listCtrl = require('../controllers/list')
const auth = require('../middlewares/auth')

const router = express.Router()

router.get('/:userId', auth, listCtrl.getAll)
router.post('/:userId', auth, listCtrl.add)
router.delete('/:userId/:listId', auth, listCtrl.delete)

module.exports = router
