const express = require('express')
const taskCtrl = require('../controllers/task')
const auth = require('../middlewares/auth')

const router = express.Router()

router.get('/:userId/:listId', auth, taskCtrl.getAll)
router.post('/:userId/:listId', auth, taskCtrl.add)
router.delete('/:userId/:taskId', auth, taskCtrl.delete)

module.exports = router
