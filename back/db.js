const { Sequelize, DataTypes } = require('sequelize');
const User = require('./models/user.js')
const List = require('./models/list.js')
const Task = require('./models/task.js')

// connect database
const sequelize = new Sequelize('test_technique_db', 'db_user', 'db_password', {
  host: 'db',
  dialect: 'mariadb'
})

// instance tables
exports.User = User(sequelize, DataTypes)
exports.List = List(sequelize, DataTypes)
exports.Task = Task(sequelize, DataTypes)

// set foreign keys
exports.User.hasMany(exports.List, { onDelete: 'cascade', onUpdate: 'cascade' })
exports.List.belongsTo(exports.User, { onDelete: 'cascade', onUpdate: 'cascade' })
exports.List.hasMany(exports.Task, { onDelete: 'cascade', onUpdate: 'cascade' })
exports.Task.belongsTo(exports.List, { onDelete: 'cascade', onUpdate: 'cascade' })

// write tables
sequelize.sync()
