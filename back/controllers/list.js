const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const List = require('../db').List

const addListDb = (data, userId) => {
	return List.create({
		name: data.name,
		userId: userId
	})
	.then (() => {
		console.log(`Added list ${data.name}`)
	})
	.catch (err => {
		console.log(err)
		return err
	})
}

const decodeError = (err) => {
	switch (err.original.code) {
	case 'ER_DUP_ENTRY':
		return "l est déjà utilisé"
	default:
		return err
	}
}
const validateForm = async (form, res, userId) => {
	if (! /^[\s\S]{0,255}$/.test(form.name)) {
		res.status(400).json({ message: 'format' })
		return false
	}
	return List.findAll({
			where: { userId: userId },
		})
		.then (lists => {
			if (lists.some(list => list.dataValues.name === form.name)) {
				res.status(400).json({ message: 'title' })
				return false
			}
			else
				return true
		})
		.catch (err => {
			console.error(err)
			res.status(500).json({})
			return false
		})
}

exports.add = async (req, res, next) => {
	if (! await validateForm(req.body, res, req.params.userId))
		return
	let r = await addListDb(req.body, req.params.userId)
	.catch (err => {
		console.log(err)
		res.status(500).json({})
	})
	if (r instanceof Error)
		res.status(401).json({})
	else
		res.status(201).json({})
}

exports.getAll = (req, res, next) => {
	List.findAll({
		
		where: { userId: req.params.userId }, // fail silently if list doesn't belong to userId
		order: [['createdAt', 'DESC']]
	})
	.then (data => {
		if (data)
			res.status(200).json({data})
		else
			res.status(401).json( { message: 'bad' })
		})
		.catch (err => {console.error(err); res.status(500).json({})})
}

exports.delete = (req, res, next) => {
	List.destroy({
		where: {
			id: req.params.listId,
			userId: req.params.userId // fail silently if list doesn't belong to userId
		},
	})
	.then (data => {
		if (data)
			res.status(200).json({data})
		else
			res.status(401).json({})
		})
		.catch (err => {console.error(err); res.status(500).json({})})
}
