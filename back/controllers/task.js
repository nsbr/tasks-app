const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const Task = require('../db').Task
const List = require('../db').List
const User = require('../db').User

const addTaskDb = (data, listId) => {
	return Task.create({
		name: data.name,
		shortDesc: data.shortDesc,
		longDesc: data.longDesc,
		date: data.date,
		listId: listId
	})
	.then (() => {
		console.log(`Added task ${data.name}`)
	})
	.catch (err => {
		console.log(err)
		return err
	})
}

const decodeError = (err) => {
	switch (err.original.code) {
	case 'ER_DUP_ENTRY':
		return "l est déjà utilisé"
	default:
		return err
	}
}
const validateForm = async (form, res) => {
	if (
			 ! /^[\s\S]{1,100}$/.test(form.name)
		|| ! /^[\s\S]{1,255}$/.test(form.shortDesc)
		|| ! /^[\s\S]{0,2000}$/.test(form.longDesc)
		) {
		res.status(400).json({ message: 'format' })
		return false
	}
	else
		return true
}

exports.add = async (req, res, next) => {
	if (! await validateForm(req.body, res))
		return
	let r = await addTaskDb(req.body, req.params.listId)
	.catch (err => {
		console.log(err)
		res.status(500).json({})
	})
	if (r instanceof Error)
		res.status(401).json({})
	else
		res.status(201).json({})
}

exports.getAll = (req, res, next) => {
	Task.findAll({
		where: { listId: req.params.listId },
		include: { // request fail silently if task doesn't belong to userId
			model: List,
			where: { userId: req.params.userId }
		},
		order: [['createdAt', 'DESC']]
	})
	.then (data => {
		if (data)
			res.status(200).json({data})
		else
			res.status(401).json( { message: 'bad' })
		})
		.catch (err => {console.error(err); res.status(500).json({})})
}

exports.delete = async (req, res, next) => {
	try {
		r = await Task.findOne({
			where: { id: req.params.taskId },
			include: {
				model: List,
				include:  User
			}
		})
	}
	catch (err) {
			res.status(500).json({})
			return
	}
	if (r.list.userId != req.params.userId) {
		res.status(403).json({})
		return
	}
	else
		Task.destroy({
			where: { id: req.params.taskId }
		})
		.then (data => {
			if (data)
				res.status(200).json({data})
			else
				res.status(401).json( {})
			})
			.catch (err => {console.error(err); res.status(500).json({})})
}
