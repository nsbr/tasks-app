const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
db = require('../db')

const addUserDb = (data) => {
	return db.User.create({
		email: data.email,
		password: data.password,
		firstName: data.firstName,
		lastName: data.lastName
	})
	.then (() => {
		console.log(`Signed user ${data.email}`)
	})
	.catch (err => {
		console.log(`Error signing user ${data.email}: ${err.original.text}`)
		return err
	})
}

const decodeError = (err) => {
	switch (err.original.code) {
	case 'ER_DUP_ENTRY':
		return "email"
	default:
		return err.original.text
	}
}

exports.signup = (req, res, next) => {
	bcrypt.hash(req.body.password, 10)
	.then (async hash => {
		req.body.password = hash
		let r = await addUserDb(req.body)
		if (r instanceof Error)
			res.status(401).json( { message: decodeError(r) })
		else
			res.status(201).json({})
	})
	.catch (err => { console.error(err) ; res.status(500).json({}) })
}

exports.login = (req, res, next) => {
	const seed = 'Arort~DrecIvcavosFibcetweed[ewJu'
	db.User.findOne({ where: { email: req.body.email } })
	.then (data => {
		if (data) {
			data = data.dataValues
			bcrypt.compare(req.body.password, data.password)
			.then (valid => {
				if (!valid)
					res.status(401).json( {message: 'bad' })
				else
					res.status(200).json({
						userId: data.id,
						token: jwt.sign (
							{ userId: data.id },
							seed,
							{ expiresIn: '24h' }
						)
					})
			})
			.catch (err => { console.error(err) ; res.status(500).json({}) })
		}
		else
			res.status(401).json( {message: 'bad' })
	})
	.catch (err => { console.error(err) ; res.status(500).json({}) })
}
