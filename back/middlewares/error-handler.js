module.exports = (err, req, res, next) => {
	console.error(err.message ? err.message : err)
	res.status(500).send({message: 'Server Error.'})
}
