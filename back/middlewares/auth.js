const jwt = require('jsonwebtoken')
db = require('../db')

module.exports = (req, res, next) => {
	const token = req.headers.authorization.split(' ')[1]
	const seed = 'Arort~DrecIvcavosFibcetweed[ewJu'
	jwt.verify(token, seed, (err, decoded) => {
		if (
			err
			|| req.body.userId && req.body.userId != decoded.userId
			|| req.params.userId && req.params.userId != decoded.userId
		)
			res.status(403).send({})
		else
			next()
	})
}
