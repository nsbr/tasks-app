const express = require('express')

const authRoutes = require('./routes/auth')
const listRoutes = require('./routes/list')
const taskRoutes = require('./routes/task')

const errorHandler = require('./middlewares/error-handler')

const app = express()

// set HTTP headers
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content, Accept, Content-Type, Authorization')
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTION')
	next()
})

// parse request body
app.use(express.json())

app.use('/api/auth', authRoutes)
app.use('/api/list', listRoutes)
app.use('/api/task', taskRoutes)

// catch asynchronous errors
app.use(errorHandler)

module.exports = app
