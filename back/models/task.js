module.exports = (sequelize, DataTypes)  =>
sequelize.define('task', {
	name: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	shortDesc: {
		type: DataTypes.STRING,
		allowNull: false
	},
	longDesc: {
		type: DataTypes.STRING(2000),
		allowNull: true
	},
	date: {
		type: DataTypes.DATE,
	},
	state: {
		type: DataTypes.BOOLEAN,
		defaultValue: false
	}
})
