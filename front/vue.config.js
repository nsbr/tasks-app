module.exports = {
	css: {
		loaderOptions: {
			sass: {
				prependData: `
					@import "@/style/variables.scss";
					@import "@/style/mixins.scss";
				`
			}
		}
	},
	publicPath: process.env.VUE_APP_BASE_URL
}
