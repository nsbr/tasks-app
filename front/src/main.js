import './style/bootstrap_custom/bootstrap_custom.min.css'
import '../node_modules/bootstrap-icons/font/bootstrap-icons.css'
import './style/style.scss'
import 'bootstrap'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

createApp(App).use(store).use(router).mount('#app')
