import { createStore } from 'vuex'

let apiUrl = process.env.VUE_APP_API_URL

export default createStore({
	state: {
		logged: null,
		lists: null,
		tasks: {}
	},
	mutations: {
		LOGIN(state, login) {
			if (typeof login === 'string')
				state.logged = JSON.parse(login)
			else
				state.logged = login
			
		},
		LOGOUT(state) {
			state.logged = null
		},
		LISTS(state, lists) {
			state.lists = lists
		},
		TASKS(state, tasks) {
			state.tasks = tasks
		}
	},
	actions: {
		login ({ commit }, login) {
			commit('LOGIN', login)
			if (typeof login === 'string')
				localStorage.setItem('login', login)
			else
				localStorage.setItem('login', JSON.stringify(login))
		},
		logout ({ commit }) {
			commit('LOGOUT')
			localStorage.setItem('login', null)

		},
		checkLogged({ commit }) {
			let login = localStorage.getItem('login')
			if (!login)
				commit('LOGOUT')
			else
				commit('LOGIN', login)
		},
		listsUpdate({ commit, state }) {
			return new Promise ((resolve, reject) => {
				const url = apiUrl + '/list/' + state.logged.userId
				fetch(url, {
					method: 'GET',
					headers: { 
						'Authorization': 'Bearer ' + state.logged.token
					}
				})
				.then (response => {
					if (response.ok)
						return(response.json())
					else
						reject(response)
				})
				.then (data => {
					commit ('LISTS', data.data)
					resolve(data.data)
				})
				.catch (err => reject(err))
			})
		},
		tasksUpdate({ commit, state }, listId) {
			return new Promise ((resolve, reject) => {
				const url = apiUrl + '/task/' + state.logged.userId + '/' +listId 
				fetch(url, {
					method: 'GET',
					headers: { 
						'Authorization': 'Bearer ' + state.logged.token
					}
				})
				.then (response => {
					if (response.ok)
						return(response.json())
					else
						reject(response)
				})
				.then (data => {
					if (data && data.error)
						console.error(data.error)
					if (data && data.message) {
						console.log(data.message)
					}
					commit ('TASKS', data.data)
					resolve(data.data)
				})
				.catch (err => reject(err))
			})
		},
		deleteList ({ state }, listId) {
			return new Promise ((resolve, reject) => {
				const url = apiUrl + '/list/' + state.logged.userId + '/' + listId 
				fetch(url, {
					method: 'DELETE',
					headers: { 
						'Accept': 'application/json', 
						'Authorization': 'Bearer ' + state.logged.token
					}
				})
				.then (response => {
					if (response.ok)
						resolve(response)
					else
						reject(response)
				})
				.catch (err => reject(err))
			})
		},
		deleteTask({ state }, taskId) {
			return new Promise ((resolve, reject) => {
				const url = apiUrl + '/task/' + state.logged.userId + '/' + taskId
				fetch(url, {
					method: 'DELETE',
					headers: { 
						'Accept': 'application/json', 
						'Authorization': 'Bearer ' + state.logged.token
					}
				})
				.then (response => {
					if (response.ok)
						resolve(response)
					else
						reject(response)
				})
				.catch (err => reject(err))
			})
		}
	}
})
